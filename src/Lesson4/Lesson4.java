package Lesson4;

public class Lesson4 {
    public static void main(String args[]) {
        System.out.println("И");
        System.out.println("true  & true  = " + (true & true));
        System.out.println("false & false = " + (false & false));
        System.out.println("true  & false = " + (true & false));
        System.out.println("false & true  = " + (false & true));
        System.out.println("ИЛИ");
        System.out.println("true  | true  = " + (true | true));
        System.out.println("false | false = " + (false | false));
        System.out.println("true  | false = " + (true | false));
        System.out.println("false | true  = " + (false | true));
        System.out.println("ИЛИ");
        System.out.println("true  ^ true  = " + (true ^ true));
        System.out.println("false ^ false = " + (false ^ false));
        System.out.println("true  ^ false = " + (true ^ false));
        System.out.println("false ^ true  = " + (false ^ true));
        System.out.println("НЕ");
        System.out.println("!true  = " + (!true));
        System.out.println("!false = " + (!false));
        System.out.println("И-НЕ");
        System.out.println("true  & true  = " + !(true & true));
        System.out.println("false & false = " + !(false & !false));
        System.out.println("true  & false = " + !(true & false));
        System.out.println("false & true  = " + !(false & true));
        System.out.println("ИЛИ-НЕ");
        System.out.println("true  | true  = " + !(true | true));
        System.out.println("false | false = " + !(false | false));
        System.out.println("true  | false = " + !(true | false));
        System.out.println("false | true  = " + !(false | true));

        System.out.println("Problem №2:");
        int a = 4;
        int b = 9;
        int c = 6;
        int d = 10;
        boolean result_2 = ((a % 2==0) & (b % 4==0)) || ((c % 3==0) & (d % 3!=0));
        System.out.println(" №2 " + (result_2));

        System.out.println("№3:");
        int f = 201;
        System.out.println ("Result №3 " + (((f & 4)==0) & ((f & 32)==0)&((f&8)==8)&((f&128)==128)|(f==100)|(f==500)|(f==0)));

        System.out.println("№4:");
        int number = 54554545;
        int digit = 4;
        System.out.println("Result №4 " + ((int)(number / (Math.pow(10, (--digit)))))%10);
    }

}
