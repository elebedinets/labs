package lesson17hw;

class Report {
    String report;
    static int i;

    public Report (String report) {
        this.report=report;
        i++;
    }

    public String getReport() {
        return i + ": " + report;
    }
}
