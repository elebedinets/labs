package lesson17hw;

public class Reporter {
    public static void main(String[] args) {
        Reporter reporter = new Reporter();
        Report report1 = new Report(" stuff");
        reporter.report(report1);
        Report report2 = new TimestampedReport(" stuff2");
        reporter.report(report2);
        Report report3 = new UsernameReport(" stuff3");
        reporter.report(report3);
    }

    public void report(Report report){
        System.out.println("My report: " + report.getReport());
    }
}
