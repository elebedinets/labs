package lesson17hw;

public class UsernameReport extends Report {

    public UsernameReport(String report) {
        super(report);
    }

    @Override
    public String getReport () {
        return i + ": "+ System.getProperty("user.name") + ": " + report;
    }
}
