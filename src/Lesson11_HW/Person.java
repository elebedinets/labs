package Lesson11_HW;

public class Person {
    String familyName;
    String givenName;
    String fatherName;
    Address address;
    private Person spouse;
    // нельзя сменить
    private final int birthYear;
    private Person[] children;

    //Для русских
    public Person (String familyName, String givenName, String fatherName, int birthYear) {
        this.familyName=familyName;
        this.givenName=givenName;
        this.fatherName=fatherName;
        this.birthYear=birthYear;
    }
    // Для иностранцев
    public Person (String familyName, String givenName, int birthYear) {
        this.familyName=familyName;
        this.givenName=givenName;
        this.birthYear=birthYear;
    }
    //Для исландцев
    Person (int birthYear, String givenName, String fatherName) {
        this.birthYear=birthYear;
        this.givenName=givenName;
        this.fatherName=fatherName;
    }
    //Полная информация
    public Person (String familyName, String givenName, int birthYear, Address address) {
        this.familyName=familyName;
        this.givenName=givenName;
        this.birthYear=birthYear;
        this.address=address;
    }
    //Супруг, нет детей
    public Person (String familyName, String givenName, int birthYear, Address address, Person spouse) {
        this.familyName=familyName;
        this.givenName=givenName;
        this.birthYear=birthYear;
        this.address=address;
        this.spouse = spouse;
    }
    //with children
    public Person (String familyName, String givenName, int birthYear, Address address, Person[] children) {
        this.familyName=familyName;
        this.givenName=givenName;
        this.birthYear=birthYear;
        this.address=address;
        this.children = children;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return this.address;
    }

    public void printPersonWithChildren () {
        System.out.println (familyName + " "+ givenName + " " + birthYear + " Адрес: "+
                getAddress().country+ " "+ getAddress().city + " "+ getAddress().apartment+" "+ getAddress().index);
        System.out.println("Всего детей: "+ children.length + " \n Имена детей " + givenName + ":");
        for (Person child: children) {
            System.out.println(" " + child.givenName);
        }
    }

    public Person getSpouse() {
        return spouse;
    }

    public Person[] getChildren() {
        return children;
    }

    public void setChildren(Person[] children) {
        this.children = children;
    }

    public void setSpouse(Person spouse) {
        this.spouse = spouse;
    }

}
