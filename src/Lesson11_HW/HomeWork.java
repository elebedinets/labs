package Lesson11_HW;

public class HomeWork {public static void main(String[] args) {
    System.out.println("Problem №1:");
    Address myAddress = new Address("Germany", "Muenster", 11, 414243);
    Address fatherAddress = new Address ("Russia", "St.Petersburg", 105);
    myAddress.printAddress();
    fatherAddress.printAddress();

    System.out.println("Problem №2:");
    Person daughter = new Person("Ivanova", "Samanta", 2019, fatherAddress);
    Person son = new Person("Ivanov", "John", 2017, fatherAddress);
    Person father = new Person("Ivanov", "Ivan", 1991, fatherAddress);
    Person[] ivanovsChidren =  {daughter, son};
    Person mother = new Person("Green", "Rachel",  1995, fatherAddress);
    mother.setSpouse(father);
    mother.setChildren(ivanovsChidren);
    father.setChildren(ivanovsChidren);
    mother.printPersonWithChildren();
    father.printPersonWithChildren();
    mother.getAddress().printAddress();
}
}
