package Lesson14_HW;

public class HomeWork {
    public static void main(String[] args) {
        System.out.println("Problem №1");
        BasicCat karas = new BasicCat("Karas", 4.5, 4.5);
        BasicCat tosya = new BasicCat("Tosya", 2.1, 3);
        BasicCat klepa = new BasicCat("Klepa", 1, 5.5);
        CalculateRealAge.calculateAge(karas);

        System.out.println("Problem №2");
        BasicCat.howManyKittens();

        System.out.println("Problem №3");
        BasicCat.quantityCats();
    }
}
